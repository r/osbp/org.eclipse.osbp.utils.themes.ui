/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.utils.themes.ui;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.InvalidRegistryObjectException;
import org.eclipse.core.runtime.RegistryFactory;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService.ThemeResourceType;

/**
 * @see org.eclipse.osbp.utils Bundle <code>org.eclipse.osbp.utils</code> for
 *      OSBP themes
 * @see org.eclipse.osbp.utils.themes.ui Bundle
 *      <code>org.eclipse.osbp.utils.themes.ui</code> for extension points
 *      <hr>
 *      {@link https://github.com/semanticsoft/vaaclipse/wiki/Resource-system}<br>
 *      {@link https://github.com/semanticsoft/vaaclipse/wiki/Resource-system#themes}<br>
 *      {@link https://github.com/semanticsoft/vaaclipse/wiki/Resource-system#theme-contributions}<br>
 *      {@link https://github.com/semanticsoft/vaaclipse/wiki/Resource-system#contribution-vs-inheritance}
 */
public class VaaclipseUiTheme implements Comparable<VaaclipseUiTheme>, Comparator<VaaclipseUiTheme> {

	public enum ThemeList {
		@Deprecated
		internal(new String[] {}), forWizard(new String[] { "base", "chameleon", "chameleon-legacy", "liferay", "presentation", "reindeer", "runo", "valo" }), forProduct(new String[] { "base", "chameleon", "chameleon-legacy", "liferay", "presentation", "reindeer", "runo", "valo" });

		protected final List<String> blackList;
		protected Set<VaaclipseUiTheme> availables;

		private ThemeList(String[] list) {
			blackList = Arrays.asList(list);
		}
	}

	public static VaaclipseUiTheme forText(String themeId) {
		for (VaaclipseUiTheme search : values(ThemeList.internal)) {
			if (themeId == null) {
				return search;
			} else if (search.getId().equals(themeId)) {
				return search;
			}
		}
		return null;
	}

	private final static VaaclipseUiTheme COMPARATOR = new VaaclipseUiTheme(null, null, null, null, null);

	public static Set<VaaclipseUiTheme> values(ThemeList forWhat) {
		if (forWhat.availables == null) {
			forWhat.availables = new TreeSet<VaaclipseUiTheme>(COMPARATOR);
			IExtensionRegistry registry = RegistryFactory.getRegistry();
			if (registry != null) {
				for (IConfigurationElement element : registry
						.getConfigurationElementsFor("org.eclipse.osbp.vaaclipse.theme")) {
					String contributor = element.getContributor().getName();
					String name = element.getName();
					if ("theme".equals(name)) {
						String id = element.getAttribute("id");
						if (!forWhat.blackList.contains(id)) {
							if (!MobileUiTheme.MOBILE_THEME.equals(id)) {
								String label = element.getAttribute("label");
								String description = element.getAttribute("description");
								List<URI> uris = new ArrayList<URI>();
								for (IConfigurationElement rlu : element.getChildren("resourceLocationUri")) {
									try {
										uris.add(new URI(rlu.getAttribute("uri")));
									} catch (InvalidRegistryObjectException e) {
										e.printStackTrace();
									} catch (URISyntaxException e) {
										e.printStackTrace();
									}
								}
								forWhat.availables.add(new VaaclipseUiTheme(id, label, description, contributor, uris));
							}
						}
					}
				}
			}
		}
		return forWhat.availables;
	}

	private final String id;
	private final String label;
	private final String description;
	private final String bundleId;
	private final List<URI> resourceLocationUris;
	private final String iconPath = ThemeResourceType.ICON.toString().toLowerCase() + "/";
	private final String imagePath = ThemeResourceType.IMAGE.toString().toLowerCase() + "/";

	private VaaclipseUiTheme(String id, String label, String description, String bundleId,
			List<URI> resourceLocationUris) {
		this.id = id;
		this.label = label == null ? this.id : label;
		this.description = description == null ? this.label : description;
		this.bundleId = bundleId;
		this.resourceLocationUris = resourceLocationUris;
	}

	@Override
	public int compareTo(VaaclipseUiTheme arg0) {
		int retcode = label.toLowerCase().compareTo(arg0.label.toLowerCase());
		if (retcode == 0) {
			retcode = description.toLowerCase().compareTo(arg0.description.toLowerCase());
		}
		if (retcode == 0) {
			retcode = id.toLowerCase().compareTo(arg0.id.toLowerCase());
		}
		return retcode;
	}

	@Override
	public String toString() {
		return id + " - " + label + " - " + description;
	}

	public String getValue() {
		return id + " - " + label + " - " + description;
	}
	
	public String getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return description;
	}

	public String getBundleId() {
		return bundleId;
	}

	public List<URI> getResourceLocationUris() {
		return resourceLocationUris;
	}

	public boolean hasIconsUri() {
		for (URI uri : resourceLocationUris) {
			if (uri.getPath().endsWith(iconPath)) {
				return true;
			}
		}
		return false;
	}

	public boolean hasImagesUri() {
		for (URI uri : resourceLocationUris) {
			if (uri.getPath().endsWith(imagePath)) {
				return true;
			}
		}
		return false;
	}

	public URI getIconsUri() {
		for (URI uri : resourceLocationUris) {
			if (uri.getPath().endsWith(iconPath)) {
				return uri;
			}
		}
		return null;
	}

	public URI getImagesUri() {
		for (URI uri : resourceLocationUris) {
			if (uri.getPath().endsWith(imagePath)) {
				return uri;
			}
		}
		return null;
	}

	@Override
	public int compare(VaaclipseUiTheme theme1, VaaclipseUiTheme theme2) {
		return theme1.getId().compareTo(theme2.getId());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VaaclipseUiTheme other = (VaaclipseUiTheme) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		return true;
	}
	
	
}
